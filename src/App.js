import React, { Component } from 'react'

import './App.css'

import Koala from './component/koala/koala'
import Button from './component/buttons/props-buttons'
import StateButton from './component/buttons/state-buttons'
import EventButton from './component/buttons/event-buttons'
import LoginButton from './component/buttons/rendering-button'
import LandingPage from './component/landingPage/landingPage'
import Demo from './component/landingPage/secondPage'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Tile from './component/tile/tile'
import SectionHeading from './component/sectionHeadings/sectionHeadings'
import Inputs from './component/inputs/inputs'

class App extends Component {
  render () {
    return (
      <div className='App'>
        <SectionHeading number="1" title="Component hierarchy with Colin the Koala" subtitle="Click Colins features to change their colour or click the buttons below to change his face completly." />
        <Koala/>
        <SectionHeading number="2" title="Button World" subtitle="Buttons that use props / state / eventListening and condional rendering all co-exist here" />
        <div id="buttons" className="container">
          <Button text="Button text from props in render..."/>
          <StateButton />
          <EventButton />
          <LoginButton />
        </div>
        <SectionHeading number="3" title="Input + Input = Input" subtitle="The very beginning of my React learning experience I was faced with the following, create a component thats adds two input values and give us the total value.  " />
        <Inputs totalText="Total" add="ADD"/>
        <SectionHeading number="4" title="Putting it all together" subtitle="This component, which is conveniently named LandinPage is a combination of eveything I have learned as well as implementing the React Router. Just click the 'Link Header' button to change the page" />
        <Router>
          <div>
            <Route exact path="/" component={LandingPage}/>
            <Route path="/secondPage" component={Demo}/>
          </div>
        </Router>
        <SectionHeading number="5" title="React and Firebase" subtitle="The last piece of the puzzle from my learnings was to implement Firebase and use it to store data. React would then take this data to change a state. " />
        <div className='container'>
          <div className="row">
            <div className="col-sm-12">
                <div className='tile-page'>
                  <Tile class={"tile-1"}/>
                  <Tile class={"tile-2"} />
                  <Tile class={"tile-3"} />
                </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default App
