import React, { Component } from 'react'

import './landingPage.css';

import Header from './neighbourhood/header/header'
import SectionA from './neighbourhood/sectionA/sectionA'
import SectionB from './neighbourhood/sectionB/sectionB'
import SectionC from './neighbourhood/sectionC/sectionC'
import Footer from './neighbourhood/footer/footer'

class LandingPage extends Component {
    render() {
        return (
            <div className='landing-page'>
                <div className='container'>
                {/* all components go here */}
                 <Header />
                 <SectionA />
                 <SectionB />
                 <SectionC />
                 <Footer />
                </div>
            </div>
        )
    }
}

export default LandingPage
