import React, { Component } from 'react'

import './sectionB.css'
import ArticleCover from '../blocks/articleCover/articleCover'
import Description from '../blocks/description/description'

class SectionB extends Component {
    render() {
        return ( 
            <section className='section-b'>
                <div className="row">
                    <div className="col-md-6 transparentClass">
                        <ArticleCover />
                    </div>
                    <div className="col-md-6">
                        <Description class="paragraphB" />
                    </div>
                </div>
            </section>
        )
    }
}

export default SectionB