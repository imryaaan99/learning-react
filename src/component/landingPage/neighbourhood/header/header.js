import React, { Component } from 'react'

import HNavigation from '../blocks/navigation/navigation'
import CTA from '../blocks/CTA/cta'
import Sidebar from '../blocks/sidebar/sidebar'

import './header.css'

class Header extends Component {
    render() {
        return ( 
            <section className='header'>
                <HNavigation class="headerNavigation" brand="Brand Header" link="Link Header"/>
                <CTA title="Developer Bot for slack" subtitle="one article a day to a random slack user"/>
                <Sidebar />
            </section>
        )
    }
}

export default Header