import React, { Component } from 'react'

import HNavigation from '../blocks/navigation/navigation'
import './footer.css'

class Footer extends Component {
    render() {
        return ( 
            <section className='footer'>
                <HNavigation class="footerNavigation" brand="Brand Footer" link="Link Footer"/>
            </section>
        )
    }
}

export default Footer