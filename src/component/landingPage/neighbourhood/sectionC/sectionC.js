import React, { Component } from 'react'

import CTA from '../blocks/CTA/cta'
import './sectionC.css'

class SectionC extends Component {
    render() {
        return ( 
            <section className='section-c'>
                <div className="row">
                   <div className="col-md-12">
                    <CTA title="Developer Bot for slack part 2" subtitle="This is the second subtitle"/>
                   </div>
                </div>
            </section>
        )
    }
}

export default SectionC