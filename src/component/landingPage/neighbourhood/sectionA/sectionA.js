import React, { Component } from 'react'

import './sectionA.css'
import Laptop from '../blocks/laptop/laptop'
import Description from '../blocks/description/description'

class SectionA extends Component {
    render() {
        return ( 
            <section className='section-a'>
                <div className="row">
                    <div className="col-md-6">
                        <Description class="paragraphA"/>
                    </div>
                    <div className="col-md-6 transparentClass">
                        <Laptop />
                    </div>
                </div>
            </section>
        )
    }
}

export default SectionA