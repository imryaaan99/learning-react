import React, { Component } from 'react'

import './cta.css'

import CTAsubtitle from '../houses/subtitle/subtitle'
import CTAtitle from '../houses/title/title'
import CTAbutton from '../houses/button/button'

class CTA extends Component {
    render() {
        return ( 
            <div className='headerCTA'>
                <div className="row">
                   <div className="col-md-12">
                        <CTAtitle title={this.props.title} />
                        <CTAsubtitle subtitle={this.props.subtitle}/>
                        <CTAbutton addtext='Add to Slick'/>
                   </div>
                </div>  
            </div>
        )
    }
}

export default CTA