import React, { Component } from 'react'

import Heading from '../houses/heading/heading'
import Divider from '../houses/divider/divider'
import Paragraph from '../houses/paragraph/paragraph'


class Description extends Component {
    render() {
        return ( 
            <div className={this.props.class}>
                <div className="row">
                    <div className="col-md-12">
                        <Heading heading="Dev bot heading here" />
                    </div>
                </div>
                <Divider borderWidth="2px" borderColor="#bebe38" marginTop="0px" marginBottom=".5rem" width="33%" />
                <div className="row">
                    <div className="col-md-12">
                        <Paragraph />
                    </div>
                </div>
            </div>
        )
    }
}

export default Description