import React, { Component } from 'react'

import Cover from '../../../../../asset/laptop.png'

class ArticleCover extends Component {
    render() {
        return ( 
            <div className='article-cover'>
                <div className="row">
                    <div className="col-md-12">
                        <img className="coverimg" src={Cover} alt="coverimg" />
                    </div>
                </div>
            </div>
        )
    }
}

export default ArticleCover