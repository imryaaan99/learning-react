import React, { Component } from 'react'

import laptop from '../../../../../asset/laptop.png'

class Laptop extends Component {
    render() {
        return ( 
            <div className='laptop'>
                <div className="row">
                    <div className="col-md-12">
                        <img className="laptopimg" src={laptop} alt="laptop" />
                    </div>
                </div>
            </div>
        )
    }
}

export default Laptop