import React, { Component } from 'react'

import Brand from '../houses/branding/branding'
import DemoLink from '../houses/link/link'

class HNavigation extends Component {
    render() {
        return ( 
            <div className={this.props.class}>
                <div className="row">
                    <div className="col-md-8 offset-md-1">
                        <Brand brand={this.props.brand} />
                   </div>
                    <div className="col-md-3">
                        <DemoLink link={this.props.link}/>
                   </div>
                </div>
            </div>
        )
    }
}

export default HNavigation