import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class DemoLink extends Component {
    render() {
        return ( 
            <div className='link'>
                <Link to="/secondPage">
                    <span>{this.props.link}</span>
                </Link>
            </div>
        )
    }
}

export default DemoLink