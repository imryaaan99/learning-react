import React, { Component } from 'react'

class CTAsubtitle extends Component {
    render() {
        return ( 
            <div className='CTAsubtitle'>
                <div className="row">
                   <div className="col-md-12">
                        <p className="text-center">{this.props.subtitle}</p>
                   </div>
                </div>
            </div>
        )
    }
}

export default CTAsubtitle