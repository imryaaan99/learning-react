import React, { Component } from 'react'
import { Link } from 'react-router-dom'


class Brand extends Component {
    render() {
        return ( 
            <div className='branding'>
                <Link to="/">
                    <span>{this.props.brand}</span>
                </Link>
            </div>
        )
    }
}

export default Brand