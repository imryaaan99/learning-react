import React, { Component } from 'react'

class CTAtitle extends Component {
    render() {
        return ( 
            <div className='CTAtitle'>
                <div className="row">
                   <div className="col-md-12">
                        <h1 className="text-center">{this.props.title}</h1>
                   </div>
                </div>
            </div>
        )
    }
}

export default CTAtitle