import React, { Component } from 'react'


class Paragraph extends Component {
    render() {
        return ( 
            <div className="paragraph">
                <div className="row">
                    <div className="col-md-12">
                        <p>{this.props.paragraph}</p>
                    </div>
                </div>
            </div>
        )
    }
}

Paragraph.defaultProps = {
    paragraph: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
}

export default Paragraph