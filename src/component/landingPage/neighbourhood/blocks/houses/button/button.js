import React, { Component } from 'react'

class CTAbutton extends Component {
    render() {
        return ( 
            <div className='CTAbutton'>
                <div className="row">
                    <div className="col-md-10 offset-md-1 button_cont" align="center">
                        <a className="example_d" rel="nofollow noopener">{this.props.addtext}</a>
                   </div>
                </div>
            </div>
        )
    }
}

export default CTAbutton