import React, { Component } from 'react';

class Divider extends Component {
    render() {
        return (
            <div className="break" style={{ borderWidth: this.props.borderWidth, borderStyle: this.props.borderStyle, borderColor: this.props.borderColor, marginTop: this.props.marginTop, marginBottom: this.props.marginBottom, width: this.props.width }}></div>
        );
    }
}

Divider.defaultProps = {
    borderWidth: "1px",
    borderStyle: "solid",
    borderColor: "#d3d3d3",
    marginTop: "1rem",
    marginBottom: "1rem",
    width: "100%"

}


export default Divider;
