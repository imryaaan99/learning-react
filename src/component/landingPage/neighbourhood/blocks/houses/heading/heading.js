import React, { Component } from 'react'


class Heading extends Component {
    render() {
        return ( 
            <div className='heading'>
                <div className="row">
                    <div className="col-md-12">
                        <h2>{this.props.heading}</h2>
                    </div>
                </div>
            </div>
        )
    }
}

export default Heading