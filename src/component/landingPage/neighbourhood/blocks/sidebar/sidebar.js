import React, { Component } from 'react'
import sidebar from '../../../../../asset/images.png';

class Sidebar extends Component {
    render() {
        return ( 
            <div className='headerSidebar'>
                <div className="row">
                   <div className="col-md-10 offset-md-1">
                        <img className="img" src={sidebar} alt="slack"/>
                   </div>
                </div>
            </div>
        )
    }
}

export default Sidebar