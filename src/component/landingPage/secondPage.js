import React, { Component } from 'react'

import './landingPage.css';

import Header from './neighbourhood/header/header'
import Footer from './neighbourhood/footer/footer'

class Demo extends Component {
    render() {
        return (
            <div className='demo-page'>
                <div className='container'>
                {/* all components go here */}
                 <Header />
                 <Footer />
                </div>
            </div>
        )
    }
}

export default Demo
