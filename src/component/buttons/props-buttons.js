import React, { Component } from 'react';

import ButtonParagraph from './buttonParagraph'

class Button extends Component {
    render() {
        return (
            <div>
                <ButtonParagraph buttonparagraph="These two buttons below resemble the ways you can pass props to a component, either through the render method or through the default props."/>
                <div className="row">
                    <div className="col-md-6">
                        <div className="button_cont" align="center">
                            <a className="example_d" rel="nofollow noopener">{this.props.headerProp}</a>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="button_cont" align="center">
                            <a className="example_d" rel="nofollow noopener">{this.props.text}</a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Button.defaultProps = {
    headerProp: "Button text from default props..."
}

export default Button;
