import React, { Component } from 'react';

import ButtonParagraph from './buttonParagraph'

class StateButton extends Component {
    state = { text1: "Button text in state", text2: "Second button text in state" };
    render() {
        return (
            <div>
                <ButtonParagraph buttonparagraph="These two buttons showcase the way you can set the data in the state of the component, these buttons are static data but normally state is used for changable data. " />
                <div className="row">
                    <div className="col-md-6">
                        <div className="button_cont" align="center">
                            <a className="example_d" rel="nofollow noopener">{this.state.text1}</a>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="button_cont" align="center">
                            <a className="example_d" rel="nofollow noopener">{this.state.text2}</a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


export default StateButton;
