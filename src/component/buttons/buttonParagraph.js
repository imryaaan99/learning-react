import React, { Component } from 'react';


class ButtonParagraph extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-md-12">
                   <p className="buttonpara text-center">{this.props.buttonparagraph}</p>
                </div>
            </div>
        );
    }
}

export default ButtonParagraph;
