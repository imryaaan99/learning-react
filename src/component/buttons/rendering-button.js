import React, { Component } from 'react';

import ButtonParagraph from './buttonParagraph'


class LoginButton extends Component {
    state = { isLoggedIn: false, text1:'Login', text2:'Logout' };

    Login() {
        this.setState({
            isLoggedIn: true
        })
    }

    Logout() {
        this.setState({
            isLoggedIn: false
        })
    }

    render() {
        const isLoggedIn = this.state.isLoggedIn;
        return (
            <div>
                <ButtonParagraph buttonparagraph="Here is an example of conditional rendering, using the state again and and if statment we can use this flag to determine what button should be shown and when. Again this button responds to an onClick event." />
                {isLoggedIn ? (
                    <div className="button_cont" align="center" onClick={this.Logout.bind(this)}>
                        <a className="example_d" rel="nofollow noopener">{this.state.text2}</a>
                    </div>
                ) : (
                        <div className="button_cont" align="center" onClick={this.Login.bind(this)}>
                            <a className="example_d" rel="nofollow noopener">{this.state.text1}</a>
                        </div>
                )}
            </div>
        );
    }
}


export default LoginButton;
