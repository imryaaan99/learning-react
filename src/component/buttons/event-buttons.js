import React, { Component } from 'react';

import ButtonParagraph from './buttonParagraph'

class EventButton extends Component {
    state = {clicks1: 0, clicks2: 0};
    updateButton1() {
        this.setState((prevState) => ({
            clicks1: prevState.clicks1 += 1
        }));
    }
     updateButton2() {
        this.setState((prevState) => ({
            clicks2: prevState.clicks2 += 1
        }));   
    }
    render() {
        return (
            <div>
                <ButtonParagraph buttonparagraph="Here is an example of a counter, using state to control each button count I have two sperate counters that respond to an onClick event. " />
                <div className="row">
                    <div className="col-md-6">
                        <div onClick={this.updateButton1.bind(this)} className="button_cont" align="center">
                            <a className="example_d" rel="nofollow noopener">{this.state.clicks1}</a>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div onClick={this.updateButton2.bind(this)} className="button_cont" align="center">
                            <a className="example_d" rel="nofollow noopener">{this.state.clicks2}</a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


export default EventButton;


 //traditional
//  function(* parameters go here*) {
//   
// }
 //fat arrow
// (* parameters go here *) => {
//  
// }