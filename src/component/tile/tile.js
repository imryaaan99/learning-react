/*global firebase*/

import React, { Component } from 'react'

import './tile.css'


// Initialize Firebase
const config = {
    apiKey: "AIzaSyCxw6-aHxGEl5Ql58kKlDd_z9qoGdodtho",
    authDomain: "react-test-7b5f6.firebaseapp.com",
    databaseURL: "https://react-test-7b5f6.firebaseio.com",
    projectId: "react-test-7b5f6",
    storageBucket: "react-test-7b5f6.appspot.com",
    messagingSenderId: "479992107710"
};
firebase.initializeApp(config);

const database = firebase.database();
const colors = database.ref('colors');

class Tile extends Component {
    state = { color: "#FFFFFF" };

    UpdateTile() {
        const random = Math.floor((Math.random() * 6) + 1);
        let selectedColor;

        let promise = new Promise((resolve, reject) => {
            colors.on("value", function(snapshot) {
                if (random === 1) {
                    selectedColor = snapshot.val().mint;
                } else if (random === 2) {
                    selectedColor = snapshot.val().pink;
                } else if (random === 3) {
                    selectedColor = snapshot.val().purple;
                } else if (random === 4){
                    selectedColor = snapshot.val().brown;
                } else if (random === 5) {
                    selectedColor = snapshot.val().yellow;
                } else  {
                    selectedColor = snapshot.val().blue;
                }
                resolve(selectedColor);
            });
        });
        promise.then((resolvedColor) => {
            this.setState({ color: resolvedColor });
        });

        console.log('hi');
    }
        

    render() {
        return (
                <div onClick={this.UpdateTile.bind(this)} style={{ background: this.state.color }} className={this.props.class}>
                    <div className="color-text-box">
                    <div className="color-text">
                            {this.state.color}
                        </div>
                    </div>
                </div>
        )
    }
}

export default Tile
