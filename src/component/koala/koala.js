import React, { Component } from 'react';

import Box from './box/box';


class Koala extends Component {
  render () {
    return (
      <div className='koala'>
        <Box colors={colorsObject} />
      </div>
    )
  }
}

//props objects
const colorsObject = {
  white: "#FFFFFF",
  lightGray: "#A6BECF",
  darkGray: "#819CAF",
  brown: "#BE845F",
  navy: "#27354A"
}

export default Koala
