import React, { Component } from 'react';
import './nose.css';

class Nose extends Component {
    state = {nose: this.props.colors.brown};
    updateHair() {
        this.setState({ nose: '#27354A'});
    }
    render() {
        return (
            <div onClick={this.updateHair.bind(this)} style={{ background: this.state.nose }} className="nose"></div>
        );
    }
}

export default Nose;