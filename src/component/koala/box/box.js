import React, { Component } from 'react'

import Head from './head/head'
import Nose from './nose/nose'
import { Lefteye, Righteye } from './eyes/eyes'
import { Leftear, Rightear } from './ears/ears'
import { Lefthair, Righthair } from './hair/hair'

class Box extends Component {  
  render () {
    return (
      <div className='container box'>
        <div className="box">
          <Rightear colors={this.props.colors}/>
          <Leftear colors={this.props.colors}/>
          <Righthair colors={this.props.colors}/>
          <Lefthair colors={this.props.colors}/>
          <Head text1={'hello'} colors={this.props.colors}/>
          <Lefteye colors={this.props.colors}/>
          <Righteye colors={this.props.colors}/>
          <Nose colors={this.props.colors} />
        </div>
      </div>
    )
  }
}

export default Box
