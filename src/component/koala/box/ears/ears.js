import React, { Component } from 'react'

import './ears.css'
import { LeftOuterear, RightOuterear } from './outerEars/outer-ears'
import { LeftInnerear, RightInnerear } from './innerEars/inner-ears'

class Leftear extends Component {
  render () {
    return (
      <div className='left-ear'>
        <LeftOuterear colors={this.props.colors}/>
        <LeftInnerear colors={this.props.colors} />
        </div>
    )
  }
}

class Rightear extends Component {
  render () {
    return (
      <div className='right-ear'>
        <RightOuterear colors={this.props.colors}/>
        <RightInnerear colors={this.props.colors} />
        </div>
    )
  }
}

export { Leftear, Rightear }
