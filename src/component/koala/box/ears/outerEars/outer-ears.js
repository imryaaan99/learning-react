import React, { Component } from 'react'

class LeftOuterear extends Component {
  state = { Outercolor: this.props.colors.lightGray};
  OuterColor() {
    this.setState({ Outercolor: this.props.colors.darkGray });
  }
  render () {
    return (
      <div onClick={this.OuterColor.bind(this)} style={{ background: this.state.Outercolor }} className='left-outer-ear'>
      </div>
    )
  }
}

class RightOuterear extends Component {
  state = { Outercolor: this.props.colors.lightGray };
  OuterColor() {
    this.setState({ Outercolor: this.props.colors.darkGray });
  }
  render () {
    return (
      <div onClick={this.OuterColor.bind(this)} style={{ background: this.state.Outercolor }} className='right-outer-ear'>
      </div>
    )
  }
}

export { LeftOuterear, RightOuterear }
