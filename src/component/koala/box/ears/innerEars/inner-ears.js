import React, { Component } from 'react'

class LeftInnerear extends Component {
  state = {Innercolor: this.props.colors.darkGray };
  InnerColor() {
    this.setState({ Innercolor: this.props.colors.lightGray});
  }
  render () {
    return (
      <div onClick={this.InnerColor.bind(this)} style={{ background: this.state.Innercolor }}  className='left-inner-ear'>
      </div>
    )
  }
}

class RightInnerear extends Component {
  state = { Innercolor: this.props.colors.darkGray };
  InnerColor() {
    this.setState({ Innercolor: this.props.colors.lightGray });
  }
  render () {
    return (
      <div onClick={this.InnerColor.bind(this)} style={{ background: this.state.Innercolor }}  className='right-inner-ear'>
      </div>
    )
  }
}

export { LeftInnerear, RightInnerear }
