import React, { Component } from 'react';
import './head.css';

class Head extends Component {
    state = {
        color: this.props.colors.lightGray, showHead: true, borderTopColor: "#A6BECF"};
    updateColor() {
        this.setState({ color: this.props.colors.darkGray, borderTopColor: "#819caf"  });
    }
    updateface() {
        this.setState({ showHead: false });
    }
    revertface() {
        this.setState({ showHead: true });
    }
    render() {
        const showHead = this.state.showHead;
        return (
            <div>
                {showHead ? (
                    <div onClick={this.updateColor.bind(this)} style={{ background: this.state.color }} className ="head"></div>
                ) : (
                        <div onClick={this.updateColor.bind(this)} style={{ borderTopColor: this.state.borderTopColor}} className="head-triangle"></div>
                )}
                <div className="row">
                    <div className="col-sm-12 mt-50">
                        <div className="row">
                            <div className="col-md-6">
                                <div onClick={this.updateface.bind(this)} className="button_cont" align="center">
                                    <a className="example_d" rel="nofollow noopener">{this.props.buttonText1}</a>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div onClick={this.revertface.bind(this)} className="button_cont" align="center">
                                    <a className="example_d" rel="nofollow noopener">{this.props.buttonText2}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

Head.defaultProps = {
    buttonText1: "Update Colin",
    buttonText2: "Revert Colin"
}

export default Head;