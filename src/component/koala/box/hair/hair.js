import React, { Component } from 'react'

import './hair.css'

class Lefthair extends Component {
  state = {Lefthair: this.props.colors.lightGray};
  updateHair(){
    this.setState({Lefthair: this.props.colors.darkGray});
  }
  render () {
    return (
      <div onClick={this.updateHair.bind(this)} style={{ background: this.state.Lefthair }}className='hair-left'>
      </div>
    )
  }
}

class Righthair extends Component {
  state = { Righthair: this.props.colors.lightGray };
  updateHair() {
    this.setState({ Righthair: this.props.colors.darkGray });
  }
  render() {
      return (
        <div onClick={this.updateHair.bind(this)} style={{ background: this.state.Righthair }} className='hair-right'>
          </div>
      )
  }
}

export {Lefthair, Righthair};
