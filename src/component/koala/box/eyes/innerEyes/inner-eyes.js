import React, { Component } from 'react'

class LeftInnereye extends Component {
  state = { Innercolor: this.props.colors.navy };
  updateInnerColor() {
    this.setState({ Innercolor: this.props.colors.white });
  }
  render () {
    return (
      <div onClick={this.updateInnerColor.bind(this)} style={{ background: this.state.Innercolor }} className='left-inner-eye'>
      </div>
    )
  }
}

class RightInnereye extends Component {
  state = { Innercolor: this.props.colors.navy };
  updateInnerColor() {
    this.setState({ Innercolor: this.props.colors.white });
  }
  render () {
    return (
      <div onClick={this.updateInnerColor.bind(this)} style={{ background: this.state.Innercolor }} className='right-inner-eye'>
      </div>
    )
  }
}

export { LeftInnereye, RightInnereye }
