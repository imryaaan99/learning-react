import React, { Component } from 'react'

import './eyes.css'
import { LeftOutereye, RightOutereye } from './outerEyes/outer-eyes'
import { LeftInnereye, RightInnereye } from './innerEyes/inner-eyes'

class Lefteye extends Component {
  render () {
    return (
      <div className='left-eye'>
        <LeftOutereye colors={this.props.colors}/>
        <LeftInnereye colors={this.props.colors}/>
      </div>
    )
  }
}

class Righteye extends Component {
  render () {
    return (
      <div className='right-eye'>
        <RightOutereye colors={this.props.colors}/>
        <RightInnereye colors={this.props.colors} />
        </div>
    )
  }
}

export { Lefteye, Righteye }
