import React, { Component } from 'react'

class LeftOutereye extends Component {
  state = { Outercolor: this.props.colors.white };
  updateOuterColor() {
    this.setState({ Outercolor: this.props.colors.navy });
  }
  render () {
    return (
      <div onClick={this.updateOuterColor.bind(this)} style={{ background: this.state.Outercolor }} className='left-outer-eye'>
      </div>
    )
  }
}

class RightOutereye extends Component {
  state = { Outercolor: this.props.colors.white };
  updateOuterColor() {
    this.setState({ Outercolor: this.props.colors.navy });
  }
  render () {
    return (
      <div onClick={this.updateOuterColor.bind(this)} style={{ background: this.state.Outercolor }} className='right-outer-eye'>
      </div>
    )
  }
}

export { LeftOutereye, RightOutereye }
