import React, { Component } from 'react';

import BreakLine from '../breakLine/breakLine'


import './inputs.css'

class Inputs extends Component {
    state = {input1: 0, input2: 0, totalValue: ''}

    Value = () => {
        this.setState({
            totalValue: parseInt(this.state.input1) + parseInt(this.state.input2)
        });
    }

    render() {
        return (
            <div className="container input-container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="row">
                            <div className="col-sm-10">
                                <input className=" input text-center form-control" type="number" min="0" onChange={(event) => this.setState({input1: event.target.value})}  />
                                <input className="input text-center form-control" type="number" min="0" onChange={(event) => this.setState({ input2: event.target.value })} />
                                <BreakLine borderWidth="2px"/>
                                <div className="total text-center">{this.props.totalText}:  {this.state.totalValue}</div>
                            </div>
                            <div className="col-sm-2">
                                <div className="button_cont" align="center"  onClick={this.Value}>
                                    <a className="example_d add-button" rel="nofollow noopener">{this.props.add}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Inputs;
